<?php

namespace Inchoo\ReviewMailer\Observer;

use Magento\Store\Model\ScopeInterface;

/**
 * Class SendReviewMail
 * @package Inchoo\ReviewMailer\Observer
 */
class SendReviewMail implements \Magento\Framework\Event\ObserverInterface
{


    const PATH_TO_EMAIL_TEMPLATE = 'inchoo_shop_review/review_mail_notification/notification_mail_template';

    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    protected $transportBuilder;
    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface
     */
    protected $inlineTranslation;
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\User\Model\ResourceModel\User\Collection
     */
    protected $adminCollection;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;


    /**
     * SendReviewMail constructor.
     * @param \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
     * @param \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\User\Model\ResourceModel\User\Collection $adminCollection
     */
    public function __construct(
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\User\Model\ResourceModel\User\Collection $adminCollection,
        \Psr\Log\LoggerInterface $logger
    )
    {
        $this->transportBuilder = $transportBuilder;
        $this->inlineTranslation = $inlineTranslation;
        $this->scopeConfig = $scopeConfig;
        $this->adminCollection = $adminCollection;
        $this->logger = $logger;

    }


    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return bool|void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {

        $review = $observer->getReview();

        if(!$this->validateSending($review, $observer->getIsUpdate())){
            return false;
        }

        $templateId = $this->scopeConfig->getValue(
            self::PATH_TO_EMAIL_TEMPLATE,
            ScopeInterface::SCOPE_STORE, $review->getStoreId()
        );

        $administrators = $this->adminCollection->getData();

        $adminMails = [];
        foreach ($administrators as $admin) {
            $adminMails[$admin['username']] = $admin['email'];
        }

        $this->inlineTranslation->suspend();

        try {

            $customer = $observer->getCustomer();

            $transport = $this->transportBuilder
                ->setTemplateIdentifier($templateId)
                ->setTemplateOptions(
                    [
                        'area' => \Magento\Framework\App\Area::AREA_ADMINHTML,
                        'store' => $review->getStoreId()
                    ]
                )
                ->setTemplateVars(['review' => $review, 'customer' => $customer])
                ->setFrom(['name' => $customer->getFirstname() . ' ' . $customer->getLastname(), 'email' => $customer->getEmail()])
                ->addTo('gpetarac@gmail.com')
                ->getTransport();

            $transport->sendMessage();

            $this->inlineTranslation->resume();

        }  catch (\Exception $e) {

            $this->logger->log(\Psr\Log\LogLevel::DEBUG, $e->getMessage());
        }
    }


    /**
     * Simple check if email is enabled in config
     * @return mixed
     */
    private function isEmailEnabled()
    {

        return $this->scopeConfig->getValue('inchoo_shop_review/review_mail_notification/notification_enable',
            ScopeInterface::SCOPE_STORE);
    }


    /**
     * Validate sending email
     * @param \Inchoo\ShopReview\Model\Review $review
     * @param bool $isUpdate
     * @return bool
     */
    private function validateSending(\Inchoo\ShopReview\Model\Review $review, bool $isUpdate)
    {
        if(!$review->getId() || !$this->isEmailEnabled() || $isUpdate){
            return false;
        }

        return true;

    }


}